const getMyAdds = async () => {
    const response = await fetch("https://www.donedeal.ie/ddapi/legacy/accounts/api/v5/dashboard/listings/all?limit=10&offset=0", {
        method: "GET",
        headers: {
            'Host': 'www.donedeal.ie',
            'Cookie': 'hard coded cookies'
        }
    })
    const jsonData = await response.json();
    console.log(jsonData);
}
