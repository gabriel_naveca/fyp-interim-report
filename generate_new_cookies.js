const login = async () => {
    try {
        const response = await fetch("https://www.donedeal.ie/accounts/api/v3/login/web", {
            method: "post",
            body: JSON.stringify({
                "username": "my Done Deal email",
                "password": "my Done Deal password"
            }),
            headers: {
            },
        });

        if (!response.ok) {
            throw new Error(`Login failed! HTTP status: ${response.status}`);
        }
        //generate new set of cookies
        console.log("cookies:" + response.headers.get('set-cookie'));
    } catch (error) {
        console.error('Error:', error);
    }
};
