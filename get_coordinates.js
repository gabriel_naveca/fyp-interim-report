const getLocation = async (county) => {
  const response = await fetch("https://www.donedeal.ie/ddapi/v1/areas/" + county, {
    method: "GET",
    headers: {
      'Cookie': 'DDSEISIUN=C7DBF4D1224285B0D56638F0F44DAE31'
    }
  })
  const jsonData = await response.json();

  for (var i = 0; i < jsonData.length; i++) {
      console.log(jsonData[i]);
  }
}
