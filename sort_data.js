//this code checks if the previousCars are still present in the currentCars, populates the history and moves all remaining curentCars to previousCars

import { MongoClient, ServerApiVersion } from 'mongodb';

const uri = "mongodb+srv://gabrielnavecadematos2021:pass@cars.9pyaiyw.mongodb.net/?retryWrites=true&w=majority";

// Create a MongoClient with a MongoClientOptions object to set the Stable API version
const client = new MongoClient(uri, {
    serverApi: {
        version: ServerApiVersion.v1,
        strict: true,
        deprecationErrors: true,
    }
});


async function run() {
    try {
        // Connect the client to the server	(optional starting in v4.7)
        await client.connect();

        //load previousCars to local array
        var previousCars = await client.db("myAppDB").collection("previousCars").find({}).toArray();
        console.log("The previousCars length is: " + previousCars.length);
        //drop previousCars collection
        await client.db("myAppDB").collection("previousCars").drop();
        console.log("previous Cars Dropped");
        console.log("populating the history collection & new previousCars")

        //iterate the previousCars local array
        for (var i = 0; i < previousCars.length; i++) {

            //check to see if car obj is still present in currentCars collection
            const results = await client.db("myAppDB").collection("currentCars").find({ id: previousCars[i].id }).toArray();

            if (results.length == 0) {
                //update previousCars[i] by removing Id and adding timestamp
                //console.log("car sold")
                const car = {
                    "make": previousCars[i].make, "model": previousCars[i].model, "year": previousCars[i].year, "price": previousCars[i].price, "mileage": previousCars[i].mileage,
                    "engineType": previousCars[i].engineType, "dealer": previousCars[i].dealer, "latitude": previousCars[i].latitude, "longitude": previousCars[i].longitude,
                    "publishDate": previousCars[i].publishDate, "sold": new Date()
                };
                //insert car obj in history collection
                await client.db("myAppDB").collection("history").insertOne(car);
            }
            //if it is still listed in currentCars then make sure to keep the oldest timestamp
            else {

                //console.log("results: "+JSON.stringify(results));
                try {
                    //const data = JSON.stringify(results)
                    //console.log(results[0].id);
                    const car = {
                        "id": results[0].id, "make": results[0].make, "model": results[0].model, "year": results[0].year, "price": results[0].price, "mileage": results[0].mileage,
                        "engineType": results[0].engineType, "dealer": results[0].dealer, "latitude": results[0].latitude, "longitude": results[0].longitude, "publishDate": previousCars[i].publishDate
                    };//await createObj(results.id, results.make, results.model, results.year, results.price, results.mileage, results.engineType, results.dealer, results.latitude, results.longitude, previousCars[i].publishDate);
                    //console.log(car);

                    //insert car obj in the previousCars collection
                    await client.db("myAppDB").collection("previousCars").insertOne(car);
                    //delete from curentCars to avoid duplicates
                    await client.db("myAppDB").collection("currentCars").deleteOne({ "id": previousCars[i].id });
                }
                catch (e) { 
                    console.log(e);
                }

            }

        }

        //move remaing cars (new listings) from currentCars to previousCars
        // load currentCars collection to local array
        try { var currentCars = await client.db("myAppDB").collection("currentCars").find({}).toArray(); }
        catch (e) { }
        // drop currentCars
        try { await client.db("myAppDB").collection("currentCars").drop(); }
        catch (e) { }
        console.log("currentCars Dropped");
        console.log("currentCars length / new listings: " + currentCars.length);
        console.log("moving cars from current to previous Cars");

        for (var i = 0; i < currentCars.length; i++) {

            await client.db("myAppDB").collection("previousCars").insertOne(currentCars[i]);
        }

    } finally {
        // Ensures that the client will close when you finish/error
        await client.close();
    }
}
run().catch(console.dir);
