var nextStart = 0;
do {
  //calling the Done Deal endpoint to search for dealers that match the word "motors" in Leinster
  const response = await fetch('https://www.donedeal.ie/search/api/v4/dealers/', {
    method: "POST",
    body: JSON.stringify({
      "from": nextStart,
      "terms": "motors",
      "counties": ["Leinster"]
    }),
    headers: {
      'cookies' :'same as previous'
    }
  });
  const jsonData = await response.json();
  nextStart = await jsonData.pagingCounts.nextStart;
  //print each delaer info to console
  for (var i = 0; i < jsonData.dealers.length; i++) {
    console.log(jsonData.dealers[i]);
  }

} while (nextStart != 0)
