import React, { useState, useEffect } from 'react';
import Link from '@mui/material/Link';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Button from '@mui/material/Button';
import Title from './Title';
import { Auth } from 'aws-amplify';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import Switch from '@mui/material/Switch';
import {
    FormControl,
    FilledInput,
    InputLabel,
    InputAdornment,
    IconButton,
    Box,
    TextField,
    Stack,
    Grid,
    Typography
} from '@mui/material';
import { Visibility, VisibilityOff } from '@mui/icons-material';

const HomePage = () => {
    const [jsonData, setJsonData] = useState([]);
    const [user, setUser] = useState(null);
    const [showPassword, setShowPassword] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [alignment, setAlignment] = useState('All');
    const label = { inputProps: { 'aria-label': 'Smart Add' } };

    useEffect(() => {
        const fetchUserData = async () => {
            try {
                const userInfo = await Auth.currentAuthenticatedUser();
                setUser(userInfo);

                const url = `http://localhost:3001/homepage?cognitoId=${userInfo.attributes.sub}&alignment=${alignment}`;
                //const url = `https://ch2n4erxxh.execute-api.eu-west-1.amazonaws.com/dev/homepage?cognitoId=${userInfo.attributes.sub}`;

                const response = await fetch(url, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });

                const data = await response.json();
                if (data !== false) {
                    setJsonData(data);
                }

            } catch (error) {
                console.error('Error:', error);
            }
        };

        fetchUserData();
    }, [alignment]);

    function preventDefault(event) {
        event.preventDefault();
    }

    const handleClickShowPassword = () => {
        setShowPassword((prev) => !prev);
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const handleLoginWithDoneDeal = async () => {
        try {
            //const url = 'https://ch2n4erxxh.execute-api.eu-west-1.amazonaws.com/dev/login';
            const url = 'http://localhost:3001/login';

            const response = await fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    cognitoId: user.attributes.sub, // assuming user is already defined and contains Cognito user details
                    DD_username: email,
                    DD_password: password,
                }),
            });

            if (!response.ok) {
                throw new Error(`Request failed with status: ${response.status}`);
            }
            const data = await response.json();

            //const cookies = await response.json();
            //console.log('Received cookies:', cookies);

            setJsonData(data);
        } catch (error) {
            console.error('Error:', error);
        }
    };

    const handleEmailChange = (event) => {
        setEmail(event.target.value);
    };

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    };

    const handleChange = (event, newAlignment) => {
        setAlignment(newAlignment);
    };

    const handleSwitchToggle = (addId, switchState) => async (event) => {
        try {
            console.log(`Switch with add.id ${addId} is toggled: ${event.target.checked}`);

            if (switchState === true) { // then remove from database
                const response = await fetch('http://localhost:3002/api/removeFromAutoUpdate', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({ addId }), // Sending the addId in the request body
                });

                if (!response.ok) {
                    throw new Error(`Request failed with status: ${response.status}`);
                }

                // Handle the response if needed
                const responseData = await response.json();
                console.log('Response from server:', responseData);
            } else { // if it's false then add to database
                const response = await fetch('http://localhost:3002/api/addToAutoUpdate', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({ addId }), // Sending the addId in the request body
                });

                if (!response.ok) {
                    throw new Error(`Request failed with status: ${response.status}`);
                }

                // Handle the response if needed
                const responseData = await response.json();
                console.log('Response from server:', responseData);
            }

            window.location.reload();
        } catch (error) {
            console.error('Error:', error);
        }
    };


    return (
        <React.Fragment>
            {jsonData.length > 0 ? (
                <>
                    <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Typography variant="h6">My Cars</Typography>
                        <ToggleButtonGroup
                            color="primary"
                            value={alignment}
                            exclusive
                            onChange={handleChange}
                            aria-label="Platform"
                        >
                            <ToggleButton value="All">All</ToggleButton>
                            <ToggleButton value="Smart">Smart</ToggleButton>
                            <ToggleButton value="Normal">Normal</ToggleButton>
                        </ToggleButtonGroup>
                    </div>
                    <Table size="small">
                        <TableHead>
                            <TableRow>
                                <TableCell>Title</TableCell>
                                <TableCell>Year</TableCell>
                                <TableCell>Engine Type</TableCell>
                                <TableCell>Mileage</TableCell>
                                <TableCell>Price</TableCell>
                                <TableCell></TableCell>
                                <TableCell align="right"></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {jsonData.map((car, index) => (
                                <TableRow key={index}>
                                    <TableCell>{car.header}</TableCell>
                                    <TableCell>2023</TableCell>
                                    <TableCell>2.0 Petrol</TableCell>
                                    <TableCell>0 Km</TableCell>
                                    <TableCell>{car.price}</TableCell>
                                    <TableCell><Switch {...label} checked={car.smartAdd} size="small"
                                        onChange={(event) => handleSwitchToggle(car.id, car.smartAdd)(event)} /></TableCell>
                                    <TableCell>
                                        <Button align="right" variant="outlined" sx={{ mr: 0 }} href={car.ddurl} target="_blank">
                                            DoneDeal
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                    <Link color="primary" href="#" onClick={preventDefault} sx={{ mt: 3 }}>
                        See more orders
                    </Link>
                </>
            ) : (
                // Display password input form when data is false
                <Grid container spacing={2}>
                    <Grid item xs={12} md={6}>
                        <Box
                            component="form"
                            sx={{
                                '& > :not(style)': { m: 1, width: '100%' },
                            }}
                            noValidate
                            autoComplete="off"
                        >
                            <TextField id="filled-basic" label="Email" variant="filled"
                                value={email}
                                onChange={handleEmailChange} />
                        </Box>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <FormControl sx={{ m: 1, width: '100%' }} variant="filled">
                            <InputLabel htmlFor="filled-adornment-password">Password</InputLabel>
                            <FilledInput
                                id="filled-adornment-password"
                                type={showPassword ? 'text' : 'password'}
                                value={password}
                                onChange={handlePasswordChange} // Update password state on change
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={handleMouseDownPassword}
                                            edge="end"
                                        >
                                            {showPassword ? <VisibilityOff /> : <Visibility />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                        <Stack direction="row" spacing={2}>
                            <Button variant="contained" color="success"
                                onClick={handleLoginWithDoneDeal}>
                                Login with Done Deal Account
                            </Button>
                        </Stack>
                    </Grid>
                </Grid>
            )}
        </React.Fragment>
    );
};

export default HomePage;
