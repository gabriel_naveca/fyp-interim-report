import Link from '@mui/material/Link';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Title from './Title';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

import React, { useState } from 'react';

const DealerSearch = () => {
    const [dealerInfo, setDealerInfo] = useState([]);
    const [showroomData, setShowroomData] = useState(null);
    const [terms, setTerms] = useState('');
    const [county, setCounty] = useState('');
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);


    function preventDefault(event) {
        event.preventDefault();
    }

    const search = () => {
        setLoading(true);
        setError(null);

        fetch('https://ch2n4erxxh.execute-api.eu-west-1.amazonaws.com/dev/findDealerInfo', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ terms: terms, county: county }),
        })
            .then((response) => {
                if (!response.ok) {
                    throw new Error(`Request failed with status: ${response.status}`);
                }
                return response.json();
            })
            .then((data) => {
                setDealerInfo(data);
                setShowroomData(null);
            })
            .catch((error) => {
                console.error(error);
                setError('Error fetching dealer data.');
            })
            .finally(() => {
                setLoading(false);
            });
    };

    const handleSelect = (showroomURL, id) => {
        const parts = showroomURL.split('/');
        const dealerName = parts[parts.length - 1];

        setLoading(true);
        setError(null);

        fetch('https://ch2n4erxxh.execute-api.eu-west-1.amazonaws.com/dev/showroom', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ name: dealerName, id: id }),
        })
            .then((response) => {
                if (!response.ok) {
                    throw new Error(`Request failed with status: ${response.status}`);
                }
                return response.json();
            })
            .then((data) => {
                setShowroomData(data);
            })
            .catch((error) => {
                console.error(error);
                setError('Error fetching showroom data.');
            })
            .finally(() => {
                setLoading(false);
            });
    };

    return (
        <React.Fragment>
            <TextField
                id="terms"
                label="Name"
                variant="outlined"
                value={terms}
                onChange={(event) => setTerms(event.target.value)}
            />
            <TextField
                id="county"
                label="County"
                variant="outlined"
                value={county}
                onChange={(event) => setCounty(event.target.value)}
            />
            <Button variant="text" onClick={search}>
                Search
            </Button>

            {loading && (
                <Box sx={{ display: 'flex', justifyContent: 'center', marginTop: 2 }}>
                    <CircularProgress />
                </Box>
            )}

            {showroomData ? (
                <div>
                    <Title>Ads</Title>
                    <Table size="small">
                        <TableHead>
                            <TableRow>
                                <TableCell></TableCell>
                                <TableCell>Price</TableCell>
                                <TableCell>Median</TableCell>
                                <TableCell>Average sale price</TableCell>
                                <TableCell>Average days on market</TableCell>
                                <TableCell align="right"></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {showroomData.map((ad, index) => (
                                <TableRow key={index}>
                                    <TableCell>
                                        <div>
                                            <div>{ad.header}</div>
                                            <div>
                                                {ad.keyInfo[0]} * {ad.keyInfo[1]} * {ad.keyInfo[2]}
                                            </div>
                                        </div>
                                    </TableCell>
                                    <TableCell>{ad.price}</TableCell>
                                    <TableCell>{ad.median}</TableCell>
                                    <TableCell>{ad.averagePrice}</TableCell>
                                    <TableCell>{ad.averageDaysOnMarket}</TableCell>
                                    <TableCell>
                                        <Button
                                            align="right"
                                            variant="outlined"
                                            sx={{ mr: 0 }}
                                            href={ad.friendlyUrl}
                                            target="_blank"
                                        >
                                            DoneDeal
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </div>
            ) : (
                <Table size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell>Address</TableCell>
                            <TableCell align="right"></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {dealerInfo.map((dealer, index) => (
                            <TableRow key={index}>
                                <TableCell>{dealer.name}</TableCell>
                                <TableCell>{dealer.address}</TableCell>
                                <TableCell>
                                    <Button
                                        align="right"
                                        variant="outlined"
                                        onClick={() => handleSelect(dealer.showroomUrl, dealer.id)}
                                    >
                                        Select
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            )}

            <Link color="primary" href="#" onClick={preventDefault} sx={{ mt: 3 }}>
                See more orders
            </Link>
        </React.Fragment>
    );
};

export default DealerSearch;
