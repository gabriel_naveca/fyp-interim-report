//this code populates the currentCars collection with all cars from Done Deal matching the filters
//run this code first followed by sortData.js

import fetch from 'node-fetch';
import { MongoClient, ServerApiVersion } from 'mongodb';

const uri = "mongodb+srv://gabrielnavecadematos2021:pass@cars.9pyaiyw.mongodb.net/?retryWrites=true&w=majority";

// Create a MongoClient with a MongoClientOptions object to set the Stable API version
const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  }
});


async function run(myobj) {
  try {
    // Connect the client to the server
    await client.connect();

    //create car obj and insert into currentCars collection
    const data = await createObj(myobj.id, myobj.displayAttributes[0].value, myobj.displayAttributes[1].value, myobj.keyInfo[0], myobj.price, myobj.keyInfo[2], myobj.keyInfo[1], myobj.dealer.name,
      myobj.dealer.latitude, myobj.dealer.longitude, new Date(myobj.publishDate));
    console.log(data);

    if(data != null){
      await client.db("myAppDB").collection("currentCars").insertOne(data);
      console.log("You successfully added one car to MongoDB!");
    } 
  }
  catch (err) {
    console.log(err);
  }
}

async function createObj(id, make, model, year, price, mileage, engineType, dealer, latitude, longitude, publishDate) {

  if (make != "" && make != null && model != "" && model != null && year != "" && year != null && price != "" && price != null && mileage != "" && mileage != null &&
    engineType != "" && engineType != null && dealer != "" && dealer != null && latitude != "" && latitude != null && longitude != "" && longitude != null && publishDate != "" && publishDate != null) {

    const data = { "id": id, "make": make, "model": model, "year": year, "price": price, "mileage": mileage, "engineType": engineType, "dealer": dealer, "latitude": latitude, "longitude": longitude, "publishDate": publishDate };
    return data;
  }
  else{
    return null
  }

}

//fetch all cars from done deal api
const getAllCars = async () => {
  var nextFrom = 0;
  do {
    const response = await fetch("https://www.donedeal.ie/ddapi/v1/search", {
      method: "post",
      body: JSON.stringify({
        "filters": [
          {
            "name": "sellerType",
            "values": [
              "pro"
            ]
          }
        ],
        "ranges": [],
        "makeModelFilters": [],
        "paging": {
          "pageSize": 40,
          "from": nextFrom
        },
        "sections": [
          "cars"
        ],
        "terms": "",
        "sort": "publishdatedesc"
      }),
      headers: {
        'Host': 'www.donedeal.ie',
        'Sec-Ch-Ua': '"Chromium";v="117", "Not;A=Brand";v="8"',
        'Version': '1.0.1696406101',
        'Sec-Ch-Ua-Mobile': '?0',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.132 Safari/537.36',
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Brand': 'donedeal',
        'Platform': 'web',
        'Sec-Ch-Ua-Platform': '"Windows"',
        'Origin': 'https://www.donedeal.ie',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Dest': 'empty',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.9'
      }
    })
    const jsonData = await response.json();

    for (var i = 0; i < 40; i++) {
      run(jsonData.ads[i]);
    }

    nextFrom = jsonData.paging.nextFrom
  }
  //lopp that controls pages (multiples of 40)
  while (nextFrom != 0) {
    //client.close();
  }
}

getAllCars();
