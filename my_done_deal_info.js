const getAccInfo = async () => {
    //endpoint to get my account information using hard coded cookies
    const response = await fetch("https://www.donedeal.ie/ddapi/legacy/accounts/api/v5/dashboard/counts", {
        method: "GET",
        headers: {
            'Cookie': 'hard coded cookies'
        }
    })
    const jsonData = await response.json();
    console.log(jsonData);
}
