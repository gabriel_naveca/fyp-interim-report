import express from 'express';
import { MongoClient } from 'mongodb';
import cors from 'cors';

const app = express();
const port = 3002;

app.use(cors());
app.use(express.json());

const uri = "mongodb+srv://gabrielnavecadematos2021:pass@cars.9pyaiyw.mongodb.net/?retryWrites=true&w=majority";

async function connectToMongoDB() {
  try {
    const client = await MongoClient.connect(uri);
    console.log('Connected to MongoDB');

    //check if listing is in autoUpdate
    app.get('/api/checkId/:id', async (req, res) => {
      const id = req.params.id;
      const db = client.db('myAppDB');
      const collection = db.collection('autoUpdate');

      try {
        const data = await collection.findOne({ addId: id });
        const flag = data !== null;
        res.json({ flag });
      } catch (err) {
        console.error('Error checking ID:', err);
        res.status(500).json({ error: 'An error occurred while checking the ID.' });
      }
    });

    //check if there are existing ccokies for a user
    app.post('/api/checkUser', async (req, res) => {
      try {
        const { cognitoId } = req.body;
        const db = client.db('myAppDB');
        const collection = db.collection('users');

        const user = await collection.findOne({ cognitoId });
        const userExists = !!user;
        const DD_cookies = user ? user.DD_cookies : null; // Fetch DD_cookies if user exists

        res.json({ userExists, DD_cookies });

      } catch (err) {
        console.error('Error checking user:', err);
        res.status(500).json({ error: 'An error occurred while checking the user.' });
      }
    });

    //create an entry for a user with cookies and cookies expiry date
    app.post('/api/createUser', async (req, res) => {
      try {
        const { DD_cookies, cognitoId } = req.body;
        const db = client.db('myAppDB');
        const collection = db.collection('users');

        const existingUser = await collection.findOne({ cognitoId });

        if (existingUser) {
          res.json({ message: 'User already exists in the database.' });
        } else {
          // Extract expiry date and user ID from the DD_cookies
          const cookies = DD_cookies.split(';');
          let expiryDate = '';
          let DD_userId = '';

          cookies.forEach(cookie => {
            if (cookie.includes('Expires')) {
              const expiryMatch = cookie.match(/Expires=([^;]+)/);
              if (expiryMatch) {
                expiryDate = expiryMatch[1];
              }
            }
            if (cookie.includes('DDUS')) {
              const userIdMatch = decodeURIComponent(cookie).match(/"id":(\d+)/);
              if (userIdMatch) {
                DD_userId = userIdMatch[1];
              }
            }
          });

          const newUser = {
            cognitoId: cognitoId,
            DD_cookies: DD_cookies,
            expiryDate: expiryDate,
            DD_userId: DD_userId,
          };

          await collection.insertOne(newUser);
          res.json({ message: 'User inserted into the database successfully.' });
        }
      } catch (err) {
        console.error('Error creating user:', err);
        res.status(500).json({ error: 'An error occurred while creating the user.' });
      }
    });


    //get everage price and average days on the market for a car
    app.post('/api/mongo-stats', async (req, res) => {
      try {
        const { make, model, year } = req.body;
        const carListings = await fetchCarListings(client, make, model, year);

        const totalPrice = carListings.reduce((acc, listing) => {
          const price = parseFloat(listing.price) || 0;
          return acc + price;
        }, 0);

        const totalDaysOnMarket = carListings.reduce((acc, listing) => {
          const publishDate = new Date(listing.publishDate);
          const soldDate = new Date(listing.sold);

          if (!isNaN(publishDate.getTime()) && !isNaN(soldDate.getTime())) {
            const daysOnMarket = (soldDate - publishDate) / (1000 * 60 * 60 * 24);
            return acc + daysOnMarket;
          } else {
            return acc;
          }
        }, 0)

        const averagePrice = totalPrice / carListings.length;
        const averageDaysOnMarket = totalDaysOnMarket / carListings.length;

        res.json({
          averagePrice: averagePrice.toFixed(2),
          averageDaysOnMarket: averageDaysOnMarket.toFixed(2),
        });
      } catch (error) {
        console.error('Error calculating MongoDB stats:', error);
        res.status(500).json({ error: 'Internal Server Error' });
      }
    });

    //add a listing (boots) to autoUpdate
    app.post('/api/addToAutoUpdate', async (req, res) => {
      try {
        var { addId } = req.body;

        if (typeof addId !== 'string') {
          addId = String(addId);
        }

        const data = {
          addId: addId,
          weekly_dec_percent: "-50",
          weekend_boost_percent: "50",
          email: "email",
          pass: "pass",
          header: "Boots",
          description: "mock add",
          currency: "EUR",
          price: "2000",
          county: "Kildare",
          publisherName: "Gabriel Naveca",
          publisherPhone: "0871438109",
          emailResponse: true,
          phoneResponse: false,
          countyTown: "Newbridge",
          photos: null,
          acceptStripePayment: false,
          shippingAvailable: false,
          quantity: 1,
          video: {
            url: null
          },
          section: {
            name: "footwear",
            id: 92
          },
          attributes: [
            {
              name: "shoesize",
              value: "11"
            },
            {
              name: "shoestyle",
              value: "Mens"
            },
            {
              name: "priceOnRequest",
              value: "false"
            },
            {
              name: "geolocation",
              value: "53.17508,-6.804747"
            }
          ],
          adTypes: [
            "for-sale"
          ]

        }
        await client.db("myAppDB").collection("autoUpdate").insertOne(data);

        res.json({});

      } catch (err) {
        console.error('Error checking user:', err);
        res.status(500).json({ error: 'An error occurred while checking the user.' });
      }
    });

    //remove an add from autoUpdate
    app.post('/api/removeFromAutoUpdate', async (req, res) => {
      try {
        var { addId } = req.body;
    
        if (typeof addId !== 'string') {
          addId = String(addId);
        }
    
        const query = { addId: addId };
        const result = await client.db("myAppDB").collection("autoUpdate").deleteOne(query);
    
        if (result.deletedCount === 1) {
          res.json({ message: 'Item removed from auto-update.' });
        } else {
          res.status(404).json({ error: 'Item not found in auto-update.' });
        }
      } catch (err) {
        console.error('Error removing item from auto-update:', err);
        res.status(500).json({ error: 'An error occurred while removing the item from auto-update.' });
      }
    });



    app.listen(port, () => {
      console.log(`Server is running on port ${port}`);
    });
  } catch (err) {
    console.error('MongoDB connection error:', err);
  }
}
//get all cars matching make, model and year from the history collection
async function fetchCarListings(client, make, model, year) {
  try {
    const db = client.db('myAppDB');
    const collection = db.collection('history');

    const query = {
      make: make.toString(),
      model: model.toString(),
      year: year.toString()
    };

    const carListings = await collection.find(query).toArray();

    const numericCarListings = carListings.map((listing) => {
      try {
        if (typeof listing.price === 'string' || listing.price instanceof String) {
          const price = parseFloat(listing.price.replace(/[^0-9.-]+/g, '')) || 0;

          return {
            ...listing,
            price: price,
          };
        } else {
          console.log('Non-string price value:', listing.price);

          return {
            ...listing,
            price: 0,
          };
        }
      } catch (error) {

        return {
          ...listing,
          price: 0,
        };
      }
    });

    return numericCarListings;
  } catch (error) {
    console.error('Error fetching car listings:', error);
    throw error;
  }
}

connectToMongoDB();
