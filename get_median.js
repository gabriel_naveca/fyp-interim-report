// Coordinates of cities in Ireland
const dublin = [53.3537509, -6.25295874];
const cork = [51.90013528, -8.4678629];
const limerick = [52.66802, -8.630497];
const galway = [53.27436453, -9.049220414];
var cities = [dublin, cork, limerick, galway]; // Array containing city coordinates

// Function to calculate median price of a car in different locations
const getMedian = async (carId, year, make, model) => {
  // Array to store prices from each city
  const cityPrices = [];

  // Loop through each city to fetch car prices
  for (var z = 0; z < cities.length; z++) {
    const prices = []; // Array to store prices for the specified car in a city
    var flag = false; // Flag to check if car is found in a city

    // Fetch data from DoneDeal API for a specific city and specified car details
    var nextFrom = 0;
    do {
      // API request to fetch car listings
      const response = await fetch("https://www.donedeal.ie/ddapi/v1/search", {
        method: "POST",
        body: JSON.stringify({
          "geoFilter": {
            "lat": cities[z][0],
            "lon": cities[z][1],
            "rad": 25
          },
          "filters": [
            {
              "name": "sellerType",
              "values": [
                "pro"
              ]
            }
          ],
          "ranges": [
            {
              "name": "year",
              "from": year,
              "to": year
            }
          ],
          "makeModelFilters": [
            {
              "make": make,
              "model": model,
              "trim": ""
            }
          ],
          "paging": {
            "pageSize": 40,
            "from": nextFrom
          },
          "sections": [
            "cars"
          ],
          "terms": "",
          "sort": "publishdatedesc"
        }),
        headers: {
          'Host': 'www.donedeal.ie'
        }
      });

      const jsonData = await response.json();
      nextFrom = await jsonData.paging.nextFrom;

      // Process fetched data to find car prices and determine if the specified car is found
      var adsLength = 0;
      try {
        adsLength = jsonData.ads.length;
      } catch (e) {
        // Handle error if ads length is unavailable
      }

      // Loop through fetched ads to collect prices and check for the specified car
      for (var i = 0; i < adsLength; i++) {
        if (jsonData.ads[i].id == carId) {
          flag = true; // Car found in this city
        }
        try {
          prices.push({ "id": (jsonData.ads[i].id), "price": (jsonData.ads[i].price) });
        } catch (e) {
          // Handle error while collecting prices
        }
      }

    } while (nextFrom != 0);

    // If car is found in the city, calculate median and return
    if (flag == true) {
      return medianof2prices(prices);
    } else {
      cityPrices.push(prices); // Add prices to cityPrices array
    }
  }

  // If car is not found in any city, check countryside
  return countrySide(cityPrices, year, make, model);
}

// Function to fetch car prices from countryside
const countrySide = async (cityPrices, year, make, model) => {
  // Array to store prices from the countryside
  const countryPrices = [];

  // Fetch data from the countryside excluding prices from cities
  var nextFrom = 0;
  do {
    // API request to fetch car listings
    const response = await fetch("https://www.donedeal.ie/ddapi/v1/search", {
      method: "POST",
      body: JSON.stringify({
        "filters": [
          {
            "name": "sellerType",
            "values": [
              "pro"
            ]
          }
        ],
        "ranges": [
          {
            "name": "year",
            "from": year,
            "to": year
          }
        ],
        "makeModelFilters": [
          {
            "make": make,
            "model": model,
            "trim": ""
          }
        ],
        "paging": {
          "pageSize": 40,
          "from": nextFrom
        },
        "sections": [
          "cars"
        ],
        "terms": "",
        "sort": "publishdatedesc"
      }),
      headers: {
        'Host': 'www.donedeal.ie',
      }
    });

    const jsonData = await response.json();
    nextFrom = await jsonData.paging.nextFrom;

    // Process fetched data to find car prices excluding those from cities
    var adsLength = 0;
    try {
      adsLength = jsonData.ads.length;
    } catch (e) {
      // Handle error if ads length is unavailable
    }

    // Loop through fetched ads to collect prices from the countryside
    for (var y = 0; y < adsLength; y++) {
      // Add ads to array if they are not in the list of city prices
      var currCarId = JSON.stringify(jsonData.ads[y].id);
      var flag = true;

      for (var i = 0; i < cityPrices.length; i++) {
        for (var z = 0; z < cityPrices[i].length; z++) {
          if (JSON.stringify(cityPrices[i][z].id) == currCarId) {
            flag = false; // Exclude ads present in city prices
            break;
          }
        }
        if (flag == false) {
          break;
        }
      }
      if (flag == true) {
        countryPrices.push({ "id": jsonData.ads[y].id, "price": jsonData.ads[y].price });
      }
    }

  } while (nextFrom != 0);

  // Calculate median price from countryside prices
  return medianof2prices(countryPrices);
}

// Function to convert a string with commas to a number
function convertoNum(s1) {
  try {
    if (s1.includes(",")) {
      let regex = new RegExp(",", 'g');
      let result = s1.replace(regex, '');
      return (Number(result));
    } else {
      return Number(s1);
    }
  } catch (e) {
    return null;
  }
}

// Function to calculate median of an array of prices
function medianof2prices(arr) {
  var prices = [];
  for (var i = 0; i < arr.length; i++) {
    var car = convertoNum(arr[i].price);
    if (car != null) {
      prices.push(car);
    }
  }

  prices = prices.sort();

  var length = prices.length;
  var half = length / 2;

  if (length % 2 == 1) {
    // If length is odd
    var returnVal = prices[(half) - 0.5];
  }
  else {

    var s1 = (prices[half]);
    var s2 = (prices[half - 1]);
    var returnVal = (s1 + s2) / 2;
    console.log(returnVal);
    return (returnVal);
  }
}
getMedian("34982331", "2014", "Ford", "Fiesta");
// 34982331 is the add id of a listing on Done Deal
