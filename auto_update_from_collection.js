//this method gets all adds from the 'autoUpdate' colection and updates the add on Done Deal
async function getUpdateList() {
  try {
    await client.connect();

    //load autoUpdate to local array
    var instructions = await client.db("myAppDB").collection("autoUpdate").find({}).toArray();
    //iterate the array and pass each add to the updateAdd function
    for (var i = 0; i < instructions.length; i++) {
      await updateAdd(instructions[i]);
    }
  }
  catch (e) {
    console.log(e);
  }
  finally {
    await client.close();
  }
}

const updateAdd = async (s1) => {

  var json = s1;
  var addId = s1.addId;
  var weekly_dec_percent = s1.weekly_dec_percent;
  var weekend_boost_percent = s1.weekend_boost_percent;
  var email = s1.email;
  var pass = s1.pass;
  const filter = { "addId": addId };

  var remove = ["_id", "addId", "weekly_dec_percent", "weekend_boost_percent", "email", "pass"]
  for (var i = 0; i < remove.length; i++) {
    var key = remove[i];
    delete json[key];
  }
  //check todays date
  var today = new Date();
  var dayOfWeek = today.getDay();
  //if it is Friday, then pass in the weekend boost value for calculation
  try {
    if (dayOfWeek == 5) {
      //console.log("Friday!!");
      json['price'] = await calculateNewPrice(json.price, weekend_boost_percent);
    }
    else if (dayOfWeek == 1) {
      //on Mondays pass in the weekly decrement value for calculation and update the 'autoUpdate' database with the new price
      var price = await calculateNewPrice(json.price, weekly_dec_percent);
      json['price'] = price;
      const update = {
        $set: { "price": price },
      };

      await client.db("myAppDB").collection("autoUpdate").updateOne(filter, update, (err, result) => {
        if (err) {
          console.error('Error updating document:', err);
        } else {
          console.log('Document updated successfully');
        }
      });
    }
    console.log(json);
  } catch (e) {
    console.log(e);
  } finally {
    //in both cases (Friday or Monday) update the price on Done Deal by calling the adds update endpoint with the expected request body
    //this is currently using hard coded cookies for authentication
    const response = await fetch("https://www.donedeal.ie/ddapi/v1/listings/update/" + addId, {
      method: "POST",
      body: JSON.stringify(json),
      headers: {
        'Host': 'www.donedeal.ie',
        'Sec-Ch-Ua': '"Not=A?Brand";v="99", "Chromium";v="118"',
        'Version': '1.0.1696406101',
        'Sec-Ch-Ua-Mobile': '?0',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.5993.70 Safari/537.36',
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Brand': 'donedeal',
        'Platform': 'web',
        'Sec-Ch-Ua-Platform': '"Windows"',
        'Origin': 'https://www.donedeal.ie',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Dest': 'empty',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.9',
        'Cookie': 'hard coded cookies'
      }
    })
    const jsonData = await response.json();
    console.log(jsonData);
  }
}

//function to calculate the new price of the add
async function calculateNewPrice(price, percent) {
  price = await convertoNum(price);
  percent = await convertoNum(percent);
  var newPrice = price + (price * (percent / 100));
  return newPrice;
}

//convert the numbers to floats for calculations and deal with cases when a ',' is present in the string (prices > 1,000)
async function convertoNum(inputString) {
  if (typeof inputString == 'number') {
    //console.error('Input is not a string. '+typeof inputString);
    return inputString;
  }
  // Remove commas from the input string using regular expression
  const stringWithoutCommas = inputString.replace(/,/g, '');

  // Parse the modified string to a numeric value
  const numericValue = parseFloat(stringWithoutCommas);

  // Check if the parsing was successful and return the numeric value
  if (!isNaN(numericValue)) {
    return numericValue;
  } else {
    // Handle the case where parsing failed (e.g., if the input is not a valid number)
    return null;
  }
}

await getUpdateList();
