import { MongoClient, ServerApiVersion } from 'mongodb';

const uri = "mongodb+srv://gabrielnavecadematos2021:pass@cars.9pyaiyw.mongodb.net/?retryWrites=true&w=majority";

// Create a MongoClient with a MongoClientOptions object to set the Stable API version
const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  }
});
//this function adds a Done Deal listing with the details bellow to the ‘autoUpdate’ collection.
//it currently takes add Id, weekly decrement, weekend boost, user email and password as parameters
//The function is tailored to add boots type of listings. Cars have different attributes.
async function addtoCollection(addId, dec, inc, email, pass) {
  try {
    const data = {
      addId: addId,
      weekly_dec_percent: dec,
      weekend_boost_percent: inc,
      email: email,
      pass: pass,
      header: "Boots",
      description: "this is a mock add on Done Deal",
      currency: "EUR",
      price: "2000",
      county: "Kildare",
      publisherName: "Gabriel Naveca",
      publisherPhone: "0871438109",
      emailResponse: true,
      phoneResponse: false,
      countyTown: "Newbridge",
      photos: null,
      acceptStripePayment: false,
      shippingAvailable: false,
      quantity: 1,
      video: {
        url: null
      },
      section: {
        name: "footwear",
        id: 92
      },
      attributes: [
        {
          name: "shoesize",
          value: "11"
        },
        {
          name: "shoestyle",
          value: "Mens"
        },
        {
          name: "priceOnRequest",
          value: "false"
        },
        {
          name: "geolocation",
          value: "53.17508,-6.804747"
        }
      ],
      adTypes: [
        "for-sale"
      ]

    }
    await client.db("myAppDB").collection("autoUpdate").insertOne(data);
  }
  catch (e) {
    console.log(e);
  }
  finally {
    await client.close();
  }
}
