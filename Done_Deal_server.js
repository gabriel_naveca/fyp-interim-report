import express from 'express';
import cors from 'cors';
import fetch from 'node-fetch';
import getMedian from './getMedian.js';

function createData(id, title, year, engineType, mileage, price, ddurl, smartAdd) {
  return { id, title, year, engineType, mileage, price, ddurl, smartAdd };
}

const app = express();
const port = 3001;

app.use(cors());
app.use(express.json());

const findDoc = async (id) => {
  try {
    const response = await fetch(`http://localhost:3002/api/checkId/${id}`);
    const data = await response.json();
    return data.flag; 
  } catch (error) {
    console.error('Error fetching data:', error);
    return false;
  }
}

app.get('/homepage', async (req, res) => {
  try {
    const cognitoId = req.query.cognitoId;
    const alignment = req.query.alignment;
    //console.log("cognitoId: "+cognitoId);
    //console.log("cognitoId: "+alignment);

    // Make a request to your other server running on port 3002
    const Cookies = await fetch(`http://localhost:3002/api/checkUser`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ cognitoId }), // Pass the Cognito ID in the request body
    });

    if (!Cookies.ok) {
      throw new Error(`Request failed with status: ${Cookies.status}`);
    }

    const cookies = await Cookies.json();

    if (cookies.userExists) {
      //console.log(cookies.DD_cookies);
      // Make a GET request to the DD api API
      const response = await fetch(
        'https://www.donedeal.ie/ddapi/legacy/accounts/api/v5/dashboard/listings/all?limit=10&offset=0',
        {
          method: 'GET',
          headers: {
            'Host': 'www.donedeal.ie',
            'Sec-Ch-Ua': '"Not=A?Brand";v="99", "Chromium";v="118"',
            'Sec-Ch-Ua-Platform': '"Windows"',
            'Sec-Ch-Ua-Mobile': '?0',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.5993.70 Safari/537.36',
            'Accept': 'application/json',
            'Brand': 'donedeal',
            'Platform': 'web',
            'Version': '1.0.1696406101',
            'Sec-Fetch-Site': 'same-origin',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Dest': 'empty',
            'Referer': 'https://www.donedeal.ie/dashboard/myads',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en-US,en;q=0.9',
            'Cookie': cookies.DD_cookies
          }
        }
      );

      if (!response.ok) {
        throw new Error(`Request to Done Deal api failed with status: ${response.status}`);
      }

      // Parse the response as JSON
      const data = await response.json();
      const returnData = [];
      
      for (var i = 0; i < data.length; i++) {

        const currentCar = (data[i]);
        const flag = await findDoc(data[i].id);
        currentCar.smartAdd = flag;

        if(alignment == "All"){
          returnData.push(data[i]);
        }
        else if(alignment == "Smart" && flag){
          returnData.push(data[i]);
        }
        else if (alignment == "Normal" && !flag){
          returnData.push(data[i]);
        }
      }
      res.json(returnData);
    } else {
      res.json(cookies.userExists);
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'An error occurred while fetching data.' });
  }
});

//endpoint for sell smart homepage
app.post('/sellSmartHomepage', async (req, res) => {
  try {
    const cognitoId = req.query.cognitoId;
    //console.log("cognitoId: "+cognitoId);

    // Make a request to your other server running on port 3002
    const Cookies = await fetch(`http://localhost:3002/api/checkUser`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ cognitoId }), // Pass the Cognito ID in the request body
    });

    if (!Cookies.ok) {
      throw new Error(`Request failed with status: ${Cookies.status}`);
    }

    const cookies = await Cookies.json();

    if (cookies.userExists) {
      //console.log(cookies.DD_cookies);
      // Make a GET request to the Done Deal API
      const response = await fetch(
        'https://www.donedeal.ie/ddapi/legacy/accounts/api/v5/dashboard/listings/all?limit=10&offset=0',
        {
          method: 'GET',
          headers: {
            'Host': 'www.donedeal.ie',
            'Sec-Ch-Ua': '"Not=A?Brand";v="99", "Chromium";v="118"',
            'Sec-Ch-Ua-Platform': '"Windows"',
            'Sec-Ch-Ua-Mobile': '?0',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.5993.70 Safari/537.36',
            'Accept': 'application/json',
            'Brand': 'donedeal',
            'Platform': 'web',
            'Version': '1.0.1696406101',
            'Sec-Fetch-Site': 'same-origin',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Dest': 'empty',
            'Referer': 'https://www.donedeal.ie/dashboard/myads',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en-US,en;q=0.9',
            'Cookie': cookies.DD_cookies
          }
        }
      );

      if (!response.ok) {
        throw new Error(`Request to Done Deal api failed with status: ${response.status}`);
      }

      // Parse the response as JSON
      const data = await response.json();
      const returnData = [];
      for (var i = 0; i < data.length; i++) {
        if (await findDoc(data[i].id)) {
          returnData.push(createData(data[i].id, data[i].header, "2023", "2.0 Petrol", "0", data[i].price, data[i].friendlyUrl))
        }
      }
      res.json(returnData);
    } else {
      res.json(cookies.userExists);
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'An error occurred while fetching data.' });
  }
});

//endpoint for handling dealer info
app.post('/findDealerInfo', async (req, res) => {
  try {
    const { terms, county } = req.body;
    const dealerData = [];
    var nextStart = 0;

    do {
      const response = await fetch('https://www.donedeal.ie/search/api/v4/dealers/', {
        method: 'POST',
        body: JSON.stringify({
          from: nextStart,
          terms: terms,
          counties: [county],
        }),
        headers: {
          'Host': 'www.donedeal.ie',
          'Sec-Ch-Ua': '"Chromium";v="119", "Not?A_Brand";v="24"',
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json;charset=UTF-8',
          'Platform': 'web',
          'Sec-Ch-Ua-Mobile': '?0',
          'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.6045.105 Safari/537.36',
          'Sec-Ch-Ua-Platform': '"Windows"',
          'Origin': 'https://www.donedeal.ie',
          'Sec-Fetch-Site': 'same-origin',
          'Sec-Fetch-Mode': 'cors',
          'Sec-Fetch-Dest': 'empty',
          'Accept-Encoding': 'gzip, deflate, br',
          'Accept-Language': 'en-US,en;q=0.9',
          'Priority': 'u=1, i',
        },
      });

      if (!response.ok) {
        throw new Error(`Request failed with status: ${response.status}`);
      }

      const jsonData = await response.json();
      //console.log(jsonData);
      nextStart = jsonData.pagingCounts.nextStart;

      for (let i = 0; i < jsonData.dealers.length; i++) {
        dealerData.push(jsonData.dealers[i]);
      }
    } while (nextStart !== 0)

    res.json(dealerData);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'An error occurred while fetching dealer data.' });
  }
});

// endpoint to handle showroom info
app.post('/showroom', async (req, res) => {
  try {
    const { name, id } = req.body;

    var returnData = [];
    var nextFrom = 0;
    var processedAds = new Set(); // Keep track of processed ads

    do {
      const response = await fetch('https://www.donedeal.ie/ddapi/v1/search/showroom/' + name, {
        method: "POST",
        body: JSON.stringify({
          "filters": [
            {
              "name": "dealerId",
              "values": [
                id
              ]
            }
          ],
          "ranges": [],
          "makeModelFilters": [],
          "paging": {
            "pageSize": 10,
            "from": nextFrom
          },
          "sections": [
            "motor"
          ],
          "terms": "",
          "sort": ""
        }),
        headers: {
          'Host': 'www.donedeal.ie',
          'Sec-Ch-Ua': '"Chromium";v="119", "Not?A_Brand";v="24"',
          'Version': '1.0.1699368131',
          'Sec-Ch-Ua-Mobile': '?0',
          'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.6045.105 Safari/537.36',
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Brand': 'donedeal',
          'Platform': 'web',
          'Sec-Ch-Ua-Platform': '"Windows"',
          'Origin': 'https://www.donedeal.ie',
          'Sec-Fetch-Site': 'same-origin',
          'Sec-Fetch-Mode': 'cors',
          'Sec-Fetch-Dest': 'empty',
          'Accept-Encoding': 'gzip, deflate, br',
          'Accept-Language': 'en-US,en;q=0.9',
          'Priority': 'u=1, i',
        }
      });

      const jsonData = await response.json();
      nextFrom = await jsonData.paging.nextFrom;

      for (var i = 0; i < jsonData.ads.length; i++) {
        const ad = jsonData.ads[i];
        var adKey = "";

        if (ad.displayAttributes && ad.displayAttributes.length > 0 && ad.keyInfo && ad.keyInfo.length > 0) {
          adKey = `${ad.displayAttributes[0].value}-${ad.displayAttributes[1].value}-${ad.keyInfo[0]}`;
        }

        if (processedAds.has(adKey)) {
          continue; // Skip this ad, as it's already processed
        }

        // Add 'median' field to jsonData.ads[i]
        ad.median = await getMedian(ad.id, ad.keyInfo[0], ad.displayAttributes[0].value, ad.displayAttributes[1].value);

        const statsResponse = await fetch('http://localhost:3002/api/mongo-stats', {
          method: 'POST',
          body: JSON.stringify({
            make: ad.displayAttributes[0].value,
            model: ad.displayAttributes[1].value,
            year: ad.keyInfo[0]
          }),
          headers: {
            'Content-Type': 'application/json',
          }
        });

        const statsData = await statsResponse.json();
        ad.averagePrice = statsData.averagePrice;
        ad.averageDaysOnMarket = statsData.averageDaysOnMarket;

        //console.log(ad);
        returnData.push(ad);

        // Add this combination to the set of processed ads
        processedAds.add(adKey);
      }

    } while (nextFrom !== 0);

    res.json(returnData);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'An error occurred while fetching showroom data.' });
  }
});


//endpoint to handle Done Deal Login
app.post('/login', async (req, res) => {
  try {
    const { cognitoId, DD_username, DD_password } = req.body;
    //console.log(cognitoId);
    //console.log(DD_username);
    //console.log(DD_password);

    const response = await fetch('https://www.donedeal.ie/accounts/api/v3/login/web', {
      method: 'POST',
      body: JSON.stringify({
        "username": DD_username,
        "password": DD_password
      }),
      headers: {
        'Host': 'www.donedeal.ie',
        'Sec-Ch-Ua': '"Not=A?Brand";v="99", "Chromium";v="118"',
        'Version': '1.0.1696406101',
        'Sec-Ch-Ua-Mobile': '?0',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.5993.70 Safari/537.36',
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Brand': 'donedeal',
        'Platform': 'web',
        'Sec-Ch-Ua-Platform': '"Windows"',
        'Origin': 'https://www.donedeal.ie',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Dest': 'empty',
        'Referer': 'https://www.donedeal.ie/',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.9',
      }
    });

    if (!response.ok) {
      throw new Error(`Request failed with status: ${response.status}`);
    }

    const getAds = await fetch(
      'https://www.donedeal.ie/ddapi/legacy/accounts/api/v5/dashboard/listings/all?limit=10&offset=0',
      {
        method: 'GET',
        headers: {
          'Host': 'www.donedeal.ie',
          'Sec-Ch-Ua': '"Not=A?Brand";v="99", "Chromium";v="118"',
          'Sec-Ch-Ua-Platform': '"Windows"',
          'Sec-Ch-Ua-Mobile': '?0',
          'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.5993.70 Safari/537.36',
          'Accept': 'application/json',
          'Brand': 'donedeal',
          'Platform': 'web',
          'Version': '1.0.1696406101',
          'Sec-Fetch-Site': 'same-origin',
          'Sec-Fetch-Mode': 'cors',
          'Sec-Fetch-Dest': 'empty',
          'Referer': 'https://www.donedeal.ie/dashboard/myads',
          'Accept-Encoding': 'gzip, deflate, br',
          'Accept-Language': 'en-US,en;q=0.9',
          'Cookie': response.headers.get('set-cookie')
        }
      }
    );

    await fetch('http://localhost:3002/api/createUser', {
      method: 'POST',
      body: JSON.stringify({
        cognitoId: cognitoId,
        DD_cookies: response.headers.get('set-cookie')
      }),
      headers: {
        'Content-Type': 'application/json',
      }
    });


    if (!getAds.ok) {
      throw new Error(`Request to Done Deal api failed with status: ${getAds.status}`);
    }

    // Parse the response as JSON
    const data = await getAds.json();
    const returnData = [];
    for (var i = 0; i < data.length; i++) {
      returnData.push(createData(data[i].id, data[i].header, "2023", "2.0 Petrol", "0", data[i].price, data[i].friendlyUrl))
    }
    res.json(returnData);

  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'An error occurred while fetching dealer data.' });
  }
});


app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
