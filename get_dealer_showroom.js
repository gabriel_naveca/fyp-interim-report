const showroom = async (name, id) => {
    //this function return all cars from the dealership matching the name and id
    const response = await fetch('https://www.donedeal.ie/ddapi/v1/search/showroom/' + name, {
        method: "POST",
        body: JSON.stringify({
            "filters": [
                {
                    "name": "dealerId",
                    "values": [
                        id
                    ]
                }
            ],
            "ranges": [],
            "makeModelFilters": [],
            "paging": {
                "pageSize": 10,
                "from": 0
            },
            "sections": [
                "motor"
            ],
            "terms": "",
            "sort": ""
        }),
        headers: {
            'Host': 'www.donedeal.ie'
        }
    });
    const jsonData = await response.json();
}
